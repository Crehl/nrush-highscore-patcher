﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace nrush_highscore_patcher
{
    class Program
    {
        const string file = "NitronicRush.exe";
        const string backup = file + ".bak";
        const string replace = "http://nrush.crehl.uk/\x00\x00\x00";
        const int addr = 0x0039f5c0;

        static void Main(string[] args)
        {
            if (!File.Exists(file))
            {
                Console.WriteLine("Couldn't find {0}", file);
                exit();
            }

            if (File.Exists(backup))
            {
                if (!inputPrompt("{0} already exists (and will be overwritten) - continue?", backup))
                    exit();
                File.Delete(backup);
            }

            try
            {
                File.Copy(file, backup);
            }
            catch (Exception)
            {
                if (!inputPrompt("Unable to create backup file ({0})- continue?", backup))
                    exit();
            }

            Byte[] bytes = {};
            try
            {
                bytes = File.ReadAllBytes(file);
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to read {0}", file);
                exit();
            }

            System.Buffer.BlockCopy(Encoding.ASCII.GetBytes(replace.ToCharArray()), 0, bytes, addr, replace.Length);

            try
            {
                File.WriteAllBytes(file, bytes);
            }
            catch (Exception)
            {
                Console.WriteLine("Unable to write {0}", file);
                exit();
            }

            Console.WriteLine("Patching appeared to be successful!");
            exit();
        }

        static bool inputPrompt(string message, params object[] p)
        {
            Console.Write("{0} [yn]: ", String.Format(message, p));
            while (true)
            {
                ConsoleKeyInfo c = Console.ReadKey(true);

                switch (c.KeyChar)
                {
                    case 'y':
                    case 'Y':
                        Console.WriteLine(c.KeyChar);
                        return true;

                    case 'n':
                    case 'N':
                        Console.WriteLine(c.KeyChar);
                        return false;

                    default:
                        break;
                }
            }
        }

        static void exit()
        {
            Console.WriteLine("Press any key to quit...");
            Console.ReadKey();
            Environment.Exit(0);
        }
    }
}
